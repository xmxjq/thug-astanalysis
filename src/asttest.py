'''
Created on 2014-4-16

@author: xmxjq
'''

from AstAnalyzer.AstAnalyzer import AstAnalyzer
import PyV8


if __name__ == '__main__':
    a = AstAnalyzer()
    test_file = open('esprima/test.js')
    try:
        test_content = test_file.read()
        sp = a.new_script(test_content)
    finally:
        test_file.close()
        
    a.print_ast(sp.script_id)
    script = a.generate_javascript(sp.script_id)
    print sp.current_ast_with_range
    