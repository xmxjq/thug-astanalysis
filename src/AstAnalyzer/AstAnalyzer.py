'''
Created on 2014-3-25

@author: xmxjq
'''



import Script
from copy import deepcopy
import ListDict
import PyV8
import copy
import json
import logging
import uuid
log = logging.getLogger("Thug")

class GlobalTest(object):
    def test(self):
        log.debug("Test Triggered")


class AstAnalyzer(object):
    '''
    classdocs
    '''

    
    def __init__(self):
        '''
        Constructor
        ''' 
        self.ctxt = PyV8.JSContext(GlobalTest())
        self.script_dict = dict()
        self.init_esprima()
        self.init_escodegen()
        
    def enter_ctxt(self):
        self.ctxt.enter()
        
    def leave_ctxt(self):
        self.ctxt.leave()
        
    def init_esprima(self):
        self.enter_ctxt()
        esprima_file = open('esprima/esprima.js')
        try:
            self.esprima_content = esprima_file.read()
            self.ctxt.eval(self.esprima_content)
        finally:
            esprima_file.close()
            self.leave_ctxt()
    
    def init_escodegen(self):
        self.enter_ctxt()
        escodegen_file = open('escodegen/escodegen.browser.js')
        try:
            self.escodegen_content = escodegen_file.read()
            self.ctxt.eval(self.escodegen_content)
        finally:
            escodegen_file.close()
            self.leave_ctxt()
            
    def new_script(self, script):
        script_id = str(uuid.uuid1().get_hex())
        script_obj = Script.Script(script_id, script)
        self.script_dict[script_id] = script_obj
        self.parse_ast(script_id)
        self.patch_with_script_id(script_id)
        return script_obj
        
    def parse_ast(self, script_id):
        self.enter_ctxt()
        script_obj = self.script_dict[script_id]
        self.ctxt.locals.script_var = script_obj.raw_script
        self.ctxt.eval("""
            var syntax = esprima.parse(script_var, {range: false});
            var output = JSON.stringify(syntax, null, 4);
        """)
        script_obj.src_ast_code = self.ctxt.locals.output        
#        output = json.dumps(self.ctxt.locals.output)
#        self.current_ast = json.loads(output)
        script_obj.src_ast = json.JSONDecoder(object_pairs_hook=ListDict.ListDict).decode((self.ctxt.locals.output))
        script_obj.current_ast = deepcopy(script_obj.src_ast)
        self.leave_ctxt()
        
    def print_ast(self, script_id):
        script_obj = self.script_dict[script_id]
        print json.dumps(script_obj.current_ast, indent=4)
            
    def generate_javascript(self, script_id):
        self.enter_ctxt()
        if script_id not in self.script_dict.keys():
            print "No Ast, Please first parse an ast"
            self.leave_ctxt()
            return None
        else:
            script_obj = self.script_dict[script_id]
            self.ctxt.locals.ast_str = json.dumps(script_obj.current_ast)
            self.ctxt.eval("""
                var ast = JSON.parse(ast_str);
                var output_script = escodegen.generate(ast);
            """)
            output = unicode(self.ctxt.locals.output_script)
            self.leave_ctxt()
            return output
    
    def patch_with_script_id(self, script_id):
        start_str = """
        {{
            "type": "VariableDeclaration", 
            "declarations": [
                {{
                    "type": "VariableDeclarator", 
                    "id": {{
                        "type": "Identifier", 
                        "name": "__Current_Script_Var__"
                    }}, 
                    "init": {{
                        "type": "CallExpression", 
                        "callee": {{
                            "type": "Identifier", 
                            "name": "SetCurrentScript"
                        }}, 
                        "arguments": [
                            {{
                                "type": "Literal", 
                                "value": "{0}"
                            }}
                        ]
                    }}
                }}
            ], 
            "kind": "var"
        }}
        """
        
        print start_str.format(script_id)
        
        end_str = """
        {
            "type": "ExpressionStatement", 
            "expression": {
                "type": "CallExpression", 
                "callee": {
                    "type": "Identifier", 
                    "name": "SetCurrentScript"
                }, 
                "arguments": [
                    {
                        "type": "Identifier", 
                        "name": "__Current_Script_Var__"
                    }
                ]
            }
        }
        """
        start = json.JSONDecoder(object_pairs_hook=ListDict.ListDict).decode(start_str.format(script_id));
        end = json.JSONDecoder(object_pairs_hook=ListDict.ListDict).decode(end_str);
        
        self.script_dict[script_id].current_ast["body"].insert(0, start)
        self.script_dict[script_id].current_ast["body"].append(end)
        self.sync_range_with_current(script_id)
        
    def sync_range_with_current(self, script_id):
        self.enter_ctxt()
        script_obj = self.script_dict[script_id]
        self.ctxt.locals.script_var = self.generate_javascript(script_id)
        self.ctxt.eval("""
            var syntax = esprima.parse(script_var, {range: true});
            var output = JSON.stringify(syntax, null, 4);
        """)
        script_obj.src_ast_code = self.ctxt.locals.output        
#        output = json.dumps(self.ctxt.locals.output)
#        self.current_ast = json.loads(output)
        script_obj.current_ast_with_range = deepcopy(json.JSONDecoder(object_pairs_hook=ListDict.ListDict).decode((self.ctxt.locals.output)))
        self.leave_ctxt()
        
#    def patch_eval(self):
#        self.iterate_list(self.current_ast[u'body'], self._is_eval, self._do_eval)
    
    def iterate_list(self, target_list, if_fun, do_fun):
        queue = []
        for k in target_list:
            queue.append(k)
        
        while len(queue) > 0:
            a = queue.pop(0)
            
            if if_fun(a):
                do_fun(target_list, a)
            
#            print type(a)
#            print a
            if type(a) is ListDict.ListDict:
                for k in a.keys():
                    queue.append(a[k])
            elif type(a) is list:
                self.iterate_list(a, if_fun, do_fun)
    
#    def _do_eval(self, src_list, node):
#        hack_node = json.JSONDecoder(object_pairs_hook=ListDict.ListDict).decode("""
#        {
#            "type": "CallExpression", 
#            "callee": {
#                "type": "Identifier", 
#                "name": "hack"
#            }, 
#            "arguments": []
#        }
#        """);
#        hack_node[u'arguments'].append(copy.deepcopy(node[u'expression'][u'arguments'][0]))
#        node[u'expression'][u'arguments'][0] = hack_node
#    
#    def _is_eval(self, node):
#        returnValue = False        
#        try:
#            if node[u'type'] == u'ExpressionStatement'\
#                and node[u'expression'][u'type'] == u'CallExpression'\
#                and node[u'expression'][u'callee'][u'name'] == u'eval':
#                returnValue = True
#        finally:
#            return returnValue
        
            