'''
Created on 2014-6-23

@author: xmxjq
'''
from copy import deepcopy
import hashlib

class Script(object):
    '''
    a single script node used in astanalyzer.
    
    '''


    def __init__(self, script_id, raw_script):
        '''
        Constructor
        '''
        self.script_id = script_id
        self.setRawScript(raw_script)
        self.src_ast_code = None
        self.src_ast = None
        self.current_ast = None
        self.current_ast_with_range = None
        
    def setRawScript(self, raw_script):
        self.raw_script = deepcopy(raw_script)
        md5 = hashlib.md5()
        md5.update(self.raw_script)
        self.hash = md5.hexdigest()
        
    